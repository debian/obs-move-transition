obs-move-transition (3.1.1-1) unstable; urgency=medium

  * New upstream version 3.1.1.
  * debian/control: bumped Standards-Version to 4.7.0.
  * debian/copyright: added new upstream rights.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 07 Nov 2024 19:43:29 -0300

obs-move-transition (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 23 Jul 2024 09:00:22 -0300

obs-move-transition (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 11 Jun 2024 14:52:31 -0300

obs-move-transition (3.0.0-1) unstable; urgency=medium

  * New upstream version 3.0.0.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 26 May 2024 00:25:53 -0300

obs-move-transition (2.12.0-1) unstable; urgency=medium

  * New upstream version 2.12.0.
  * debian/copyright: updated upstream and packaging copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 11 May 2024 13:37:39 -0300

obs-move-transition (2.9.6-1) unstable; urgency=medium

  * New upstream version 2.9.6.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 08 Nov 2023 17:49:27 -0300

obs-move-transition (2.9.5-1) unstable; urgency=medium

  * New upstream version 2.9.5.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 15 Sep 2023 13:26:25 -0300

obs-move-transition (2.9.4-1) unstable; urgency=medium

  * New upstream version 2.9.4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 03 Sep 2023 15:25:57 -0300

obs-move-transition (2.9.1-1) unstable; urgency=medium

  * New upstream version 2.9.1.
  * debian/patches/010_update-translation-pt-BR.patch: removed because the
    upstream added the new translations to source code. Thanks!
  * debian/rules:
      - Disabled temporarily the missing translation detection routine.
      - Using 'export DEB_CFLAGS_MAINT_APPEND = -Wno-psabi' to avoid a FTBFS
        in 32-bit archs when building with GCC-12.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 14 Jun 2023 12:51:39 -0300

obs-move-transition (2.8.1-4) unstable; urgency=medium

  * debian/rules: removed temporary workaround to exclude extra installed
    files in favor of the '-DLINUX_PORTABLE=Off' argument for CMake.
  * debian/upstream/metadata: updated Donation field.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 21 Feb 2023 18:32:58 -0300

obs-move-transition (2.8.1-3) unstable; urgency=medium

  * debian/watch: using filenamemangle to generate right names for downloaded
    files.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 31 Jan 2023 21:30:33 -0300

obs-move-transition (2.8.1-2) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/patches/010_update-translation-pt-BR.patch: created to update pt_BR
    translation.
  * debian/rules: added a target to check if translation to pt_BR is updated.
  * debian/watch: improved the search rule, checking if the upstream forgotten
    to add a tag on GitHub.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 29 Jan 2023 00:11:58 -0300

obs-move-transition (2.8.1-1) unstable; urgency=medium

  * New upstream version 2.8.1.
  * debian/copyright:
      - Converted the last paragraph of the GPL-2 in a comment.
      - Updated upstream and packaging copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 10 Jan 2023 13:47:38 -0300

obs-move-transition (2.8.0-1) unstable; urgency=medium

  * New upstream version 2.8.0.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 12 Dec 2022 14:22:15 -0300

obs-move-transition (2.7.2-1) unstable; urgency=medium

  * New upstream version 2.7.2.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 25 Nov 2022 23:22:29 -0300

obs-move-transition (2.7.1-1) unstable; urgency=medium

  * New upstream version 2.7.1.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 08 Nov 2022 19:15:40 -0300

obs-move-transition (2.7.0-1) unstable; urgency=medium

  * New upstream version 2.7.0.
  * debian/control:
      - Bumped relationships from 27 to 28.0.1 for obs-studio and libobs.
      - Removed a sentence about minimum supported version for OBS in long
        description.
  * debian/rules: added a temporary workaround to remove duplicate files
    created by upstream.
  * debian/watch: updated the search rule to make it compliant with new
    standards of the GitHub.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 12 Oct 2022 17:18:37 -0300

obs-move-transition (2.6.1-1) unstable; urgency=medium

  * New upstream version 2.6.1.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 17 Jul 2022 01:48:12 -0300

obs-move-transition (2.6.0-1) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * New upstream version 2.6.0.
  * debian/control: bumped Standards-Version to 4.6.1.

  [ Debian Janitor ]
  * Remove duplicate values for fields Donation in debian/upstream/metadata.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 12 Jul 2022 15:49:52 -0300

obs-move-transition (2.5.8-1) unstable; urgency=medium

  * New upstream version 2.5.8.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 05 Mar 2022 17:03:15 -0300

obs-move-transition (2.5.7-1) unstable; urgency=medium

  * New upstream version 2.5.7.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 13 Feb 2022 22:36:22 -0300

obs-move-transition (2.5.6-1) unstable; urgency=medium

  * New upstream version 2.5.6.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 23 Jan 2022 22:14:58 -0300

obs-move-transition (2.5.5-1) unstable; urgency=medium

  * New upstream version 2.5.5.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 22 Jan 2022 19:39:58 -0300

obs-move-transition (2.5.4-1) unstable; urgency=medium

  * New upstream version 2.5.4.
  * debian/copyright: updated upstream copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 11 Jan 2022 20:47:28 -0300

obs-move-transition (2.5.3-3) unstable; urgency=medium

  * Using variables to improve and standardize the building and CI test.
    Consequently, updated the following files:
      - debian/rules
      - debian/tests/control
  * debian/upstream/metadata: added an extra Donation field.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 02 Jan 2022 23:52:19 -0300

obs-move-transition (2.5.3-2) unstable; urgency=medium

  * Happy New Year!
  * debian/control: minor fix in long description.
  * debian/copyright: updated packaging copyright years.
  * debian/upstream/metadata: added Donation field.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 01 Jan 2022 20:22:41 -0300

obs-move-transition (2.5.3-1) unstable; urgency=medium

  * New upstream version 2.5.3.
  * The upstream enabled the 'build out of tree'. Consequently:
      - debian/install: no longer needed. Removed.
      - debian/patches/*: no longer needed. Removed.
      - debian/rules:
          ~ Created override_dh_auto_configure to pass some parameters to
            cmake.
          ~ Fixed the path for the translations.
          ~ Removed no longer needed override_dh_install.
  * debian/rules: added a check to verify if the plugin was installed in the
    right place.
  * debian/tests/*: added to provide a trivial CI test. The current test checks
    if the lib loads.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 27 Dec 2021 19:30:50 -0300

obs-move-transition (2.5.2-1) unstable; urgency=medium

  * New upstream version 2.5.2.
  * debian/control:
      - Added a relationship for libobs-dev.
      - Removed a digit from Standards-Version.
  * debian/patches/010_fix-cmake.patch: fixed the upstream version.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 04 Dec 2021 09:30:12 -0300

obs-move-transition (2.5.1-3) unstable; urgency=medium

  * debian/patches/:
      - 010_fix-cmake.patch: added an include to allow the CMake to search for
        files in /usr/include/obs/. Thanks for Sebastian Ramacher.
        (Closes: #998853)
      - 030_provide-missing-files.patch: removed because the following reasons:
          ~ The last change in patch 010 (see above) fixed the issue with the
            file obs-frontend-api.h.
          ~ The upstream removed the include for easings.h in current version
            ( 2.5.1). (Closes: #998855)
            Reference: https://github.com/exeldro/obs-move-transition/issues/93
  * debian/rules: removed no longer needed dh_auto_configure.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 28 Nov 2021 12:07:48 -0300

obs-move-transition (2.5.1-2) unstable; urgency=medium

  * debian/control: fix minimum version for obs-studio in long description.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 07 Nov 2021 09:42:17 -0300

obs-move-transition (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1.
  * Upload to unstable.
  * debian/control:
     - Added Enhances field to binary section.
     - Changed minimum version for obs-studio to 27 in Depends field. The
       upstream confirmed this information.
     - Set 'Multi-Arch: same'.
  * debian/copyright: added absent years for the upstream.
  * debian/patches/: updated all current patches to run with the new upstream
    release.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 06 Nov 2021 15:24:54 -0300

obs-move-transition (2.5.0-2) experimental; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.0.1.
  * debian/rules: fixed the target variable (changed to DEB_TARGET_MULTIARCH).

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 12 Sep 2021 23:53:59 -0300

obs-move-transition (2.5.0-1) unstable; urgency=medium

  * Initial release (Closes: #993881)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 07 Sep 2021 09:07:28 -0300
